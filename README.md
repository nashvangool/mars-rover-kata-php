A simple Mars Rover application

# Functional specification

This program simulates a very simple imaginary Mars rover. The rover lands at a specific (integer) X and Y coordinate on a grid, with a heading in one of the four cardinal directions (north, east, south, west). Given a sequence of the following commands:

- Move forward by 1 unit
- Move backward by 1 unit
- Turn 90 degrees left, in-place
- Turn 90 degrees right, in-place

the program will output the coordinates and heading of the rover after performing these commands.

# Requirements

The project runs in Docker, and is run using a Makefile. Having Docker and GNU Make installed should be sufficient.

**Note:** this project was built and tested on a Linux machine. It should run fine on macOS machines as well, and
Windows too if WSL is used, but especially for Windows I cannot guarantee this!

# Usage

## Installing the runtime dependencies

```bash
$ make install
```

## Updating the runtime dependencies

```bash
$ make update
```

## Running the test suite

```bash
$ make test
```

Code coverage reports will be output to `coverage.txt` (in plain text format) and `coverage/` (as HTML)

## Getting a list of all make targets

```bash
$ make
```

or

```bash
$ make help
```

## Running the application

**Make sure the runtime dependencies are installed first!**

The make target for running the application in Docker is a bit less convenient to use, so a convenience wrapper script
is provided under `bin/rover`. Use it as such:

```bash
$ bin/rover <X coordinate> <Y coordinate> <initial heading> [command string]
```

The X and Y coordinates should be integer numbers.
The initial should be one of `NORTH`, `EAST`, `SOUTH` or `WEST`,
with the optional shorthands `N`, `E`, `S`, and `W`.

The result of the application will be the rover's new coordinates and heading.

### Command strings

The command string should be a continuous string of the following instructions:

- `F`: move forward on current heading
- `B`: move backward on current heading
- `L`: rotate left by 90 degrees
- `R`: rotate right by 90 degrees

Any characters not in this list will be ignored.
So for instance, the command string `GO FORWARD ROVER` is parsed as `FRRRR`

#### Examples

**Setting the rover down at (0,0) facing north:**

```bash
$ bin/rover 0 0 NORTH
New position and heading: (0, 0) NORTH
```

**Setting the rover down at (1,2) facing north and turning it right 180 degrees:**

```bash
$ bin/rover 1 2 NORTH RR
New position and heading: (1, 2) SOUTH
```

**Setting the rover down at (5,1) facing east (with shorthand) and navigating it to (0,0):**

```bash
$ bin/rover 5 1 E RRFFFFFLF
New position and heading: (0, 0) SOUTH
```
