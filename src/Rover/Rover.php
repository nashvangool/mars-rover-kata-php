<?php
declare(strict_types=1);

namespace App\Rover;

use App\Rover\Navigation\CommandString;
use App\Rover\Navigation\Vector;

class Rover
{
    public function __construct(
        private Vector $vector
    ) {}

    public function runCommandString(CommandString $commandString): void
    {
        foreach ($commandString as $instruction) {
            $this->vector = $instruction->applyToVector($this->vector);
        }
    }

    public function getVector(): Vector
    {
        return $this->vector;
    }
}
