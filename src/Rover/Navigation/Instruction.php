<?php
declare(strict_types=1);

namespace App\Rover\Navigation;

interface Instruction
{
    public function applyToVector(Vector $input): Vector;
}
