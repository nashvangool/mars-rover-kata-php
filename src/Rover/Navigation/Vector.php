<?php
declare(strict_types=1);

namespace App\Rover\Navigation;

class Vector
{
    public function __construct(
        private Position $position,
        private Heading $heading
    )
    {}

    public function getPosition(): Position
    {
        return $this->position;
    }

    public function getHeading(): Heading
    {
        return $this->heading;
    }

    public function __toString(): string
    {
        return $this->position . ' ' . $this->heading;
    }
}
