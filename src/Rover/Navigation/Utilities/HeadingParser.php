<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Utilities;

use App\Rover\Navigation\Heading;

class HeadingParser
{
    public function parse(string $heading): Heading
    {
        if ($heading === '') {
            throw new \InvalidArgumentException('Empty string cannot be converted to a heading');
        }

        try {
            return match (str_split(\strtoupper($heading))[0]) {
                'N' => new Heading\North(),
                'S' => new Heading\South(),
                'E' => new Heading\East(),
                'W' => new Heading\West(),
            };
        } catch (\UnhandledMatchError) {
            throw new \InvalidArgumentException("Invalid heading string: '{$heading}'");
        }
    }
}
