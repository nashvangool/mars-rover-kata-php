<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Utilities;

use App\Rover\Navigation\CommandString;
use App\Rover\Navigation\Instruction\Back;
use App\Rover\Navigation\Instruction\Forward;
use App\Rover\Navigation\Instruction\Left;
use App\Rover\Navigation\Instruction\Right;

class CommandStringParser
{
    public function parse(string $commandString): CommandString
    {
        $instructions = [];

        foreach (str_split($commandString) as $command) {
            try {
                $instructions[] = match ($command) {
                    'F' => new Forward(),
                    'L' => new Left(),
                    'R' => new Right(),
                    'B' => new Back(),
                };
            } catch (\UnhandledMatchError) {
                continue;
            }
        }

        return new CommandString(...$instructions);
    }
}
