<?php
declare(strict_types=1);

namespace App\Rover\Navigation;

/**
 * @implements \IteratorAggregate<int, Instruction>
 */
class CommandString implements \IteratorAggregate
{
    /** @var Instruction[] */
    private array $instructions = [];

    public function __construct(Instruction ...$instructions)
    {
        $this->instructions = $instructions;
    }

    /**
     * @return \Iterator<int, Instruction>
     */
    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->instructions);
    }
}
