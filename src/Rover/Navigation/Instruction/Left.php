<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Instruction;

use App\Rover\Navigation\Instruction;
use App\Rover\Navigation\Vector;

class Left implements Instruction
{
    public function applyToVector(Vector $input): Vector
    {
        return new Vector(
            $input->getPosition(),
            $input->getHeading()->turnLeft()
        );
    }
}
