<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Instruction;

use App\Rover\Navigation\Instruction;
use App\Rover\Navigation\Vector;

class Forward implements Instruction
{
    public function applyToVector(Vector $input): Vector
    {
        return new Vector(
            $input->getHeading()->getCoordinateTransformationFactors()->applyToPosition($input->getPosition(), 1),
            $input->getHeading()
        );
    }
}
