<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Model;

use App\Rover\Navigation\Position;

class CoordinateTransformationVector
{
    public function __construct(private int $x, private int $y)
    {}

    /**
     * Get a new Position relative to the given one, moved in the direction indicated by the X and Y coordinate in this
     * vector, multiplied by the given factor. So for X and Y both being 1 and a factor of 2, the new position
     * will be 2 units right and up from the original position
     *
     * @param Position $position The starting position
     * @param int $factor The factor to apply to the displacement
     * @return Position A new Position object displaced by the given factor and the X and Y parameters in this vector
     */
    public function applyToPosition(Position $position, int $factor): Position
    {
        return new Position(
            $position->getX() + ($this->x * $factor),
            $position->getY() + ($this->y * $factor)
        );
    }
}
