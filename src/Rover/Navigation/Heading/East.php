<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Heading;

use App\Rover\Navigation\Heading;
use App\Rover\Navigation\Model\CoordinateTransformationVector;

class East implements Heading
{
    public function turnLeft(): Heading
    {
        return new North();
    }

    public function turnRight(): Heading
    {
        return new South();
    }

    public function getCoordinateTransformationFactors(): CoordinateTransformationVector
    {
        return new CoordinateTransformationVector(1, 0);
    }

    public function __toString(): string
    {
        return 'EAST';
    }
}
