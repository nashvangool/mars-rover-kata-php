<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Heading;

use App\Rover\Navigation\Heading;
use App\Rover\Navigation\Model\CoordinateTransformationVector;

class North implements Heading
{
    public function turnLeft(): Heading
    {
        return new West();
    }

    public function turnRight(): Heading
    {
        return new East();
    }

    public function getCoordinateTransformationFactors(): CoordinateTransformationVector
    {
        return new CoordinateTransformationVector(0, 1);
    }

    public function __toString(): string
    {
        return 'NORTH';
    }
}
