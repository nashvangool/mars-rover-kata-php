<?php
declare(strict_types=1);

namespace App\Rover\Navigation\Heading;

use App\Rover\Navigation\Heading;
use App\Rover\Navigation\Model\CoordinateTransformationVector;

class South implements Heading
{
    public function turnLeft(): Heading
    {
        return new East();
    }

    public function turnRight(): Heading
    {
        return new West();
    }

    public function getCoordinateTransformationFactors(): CoordinateTransformationVector
    {
        return new CoordinateTransformationVector(0, -1);
    }

    public function __toString(): string
    {
        return 'SOUTH';
    }
}
