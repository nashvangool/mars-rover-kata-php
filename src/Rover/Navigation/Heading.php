<?php
declare(strict_types=1);

namespace App\Rover\Navigation;

use App\Rover\Navigation\Model\CoordinateTransformationVector;

interface Heading
{
    public function turnLeft(): Heading;
    public function turnRight(): Heading;
    public function getCoordinateTransformationFactors(): CoordinateTransformationVector;
    public function __toString(): string;
}
