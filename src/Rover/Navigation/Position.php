<?php
declare(strict_types=1);

namespace App\Rover\Navigation;

class Position
{
    public function __construct(private int $x, private int $y)
    {}

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function __toString(): string
    {
        return \sprintf('(%d,%d)', $this->x, $this->y);
    }
}
