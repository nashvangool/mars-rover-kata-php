<?php
declare(strict_types=1);

namespace Test\Rover\Navigation;

use App\Rover\Navigation\CommandString;
use App\Rover\Navigation\Instruction\Back;
use App\Rover\Navigation\Instruction\Forward;
use App\Rover\Navigation\Instruction\Left;
use App\Rover\Navigation\Instruction\Right;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\CommandString
 */
class CommandStringTest extends TestCase
{
    public function testIterationMaintainsOrderProvidedInConstructor(): void
    {
        $commandString = new CommandString(
            new Forward(),
            new Back(),
            new Left(),
            new Right()
        );

        $this->assertEquals(
            [
                new Forward(),
                new Back(),
                new Left(),
                new Right()
            ],
            \iterator_to_array($commandString),
        );
    }
}
