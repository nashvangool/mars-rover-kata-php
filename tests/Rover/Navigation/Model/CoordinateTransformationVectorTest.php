<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Model;

use App\Rover\Navigation\Model\CoordinateTransformationVector;
use App\Rover\Navigation\Position;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Model\CoordinateTransformationVector
 */
class CoordinateTransformationVectorTest extends TestCase
{
    /**
     * @dataProvider provideDisplacementParameters
     */
    public function testPositionDisplacement(
        Position $originalPosition,
        int $x, int $y, int $factor,
        Position $expectedPosition
    ): void
    {
        $transformationVector = new CoordinateTransformationVector($x, $y);

        $this->assertEquals(
            $expectedPosition,
            $transformationVector->applyToPosition($originalPosition, $factor)
        );
    }

    /**
     * @return array<string, array{Position, int, int, int, Position}>
     */
    public function provideDisplacementParameters(): array
    {
        return [
            '(2, 2) moved by (1,1) factor 2' => [
                new Position(2, 2),
                1, 1, 2,
                new Position(4, 4)
            ],
            '(1, 5) moved by (1, -1) factor 5' => [
                new Position(1, 5),
                1, -1, 5,
                new Position(6, 0)
            ],
            '(13, -2) moved by (5, 0) factor 3' => [
                new Position(13, -2),
                5, 0, 3,
                new Position(28, -2)
            ],
            '(7, 42) moved by (0, 3) factor 2' => [
                new Position(7, 42),
                0, 3, 2,
                new Position(7, 48)
            ],
            '(19, 84) moved by (2, 2) factor -2' => [
                new Position(19, 84),
                2, 2, -2,
                new Position(15, 80)
            ]
        ];
    }
}
