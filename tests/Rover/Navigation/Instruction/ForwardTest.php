<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Instruction;

use App\Rover\Navigation\Heading\East;
use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Heading\West;
use App\Rover\Navigation\Instruction\Forward;
use App\Rover\Navigation\Position;
use App\Rover\Navigation\Vector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Instruction\Forward
 */
class ForwardTest extends TestCase
{
    /**
     * @dataProvider provideStartingVectors
     */
    public function testVectorTransformationWithStartingDirection(
        Vector $startingVector,
        Vector $expectedVector
    ): void
    {
        $forward = new Forward();

        $this->assertEquals(
            $expectedVector,
            $forward->applyToVector($startingVector)
        );
    }

    /**
     * @return array<string, array{Vector, Vector}>
     */
    public function provideStartingVectors(): array
    {
        return [
            '0,0 north' => [
                new Vector(
                    new Position(0,0),
                    new North()
                ),
                new Vector(
                    new Position(0, 1),
                    new North()
                )
            ],
            '0,0 east' => [
                new Vector(
                    new Position(0,0),
                    new East()
                ),
                new Vector(
                    new Position(1, 0),
                    new East()
                )
            ],
            '0,0 south' => [
                new Vector(
                    new Position(0,0),
                    new South()
                ),
                new Vector(
                    new Position(0, -1),
                    new South()
                )
            ],
            '0,0 west' => [
                new Vector(
                    new Position(0,0),
                    new West()
                ),
                new Vector(
                    new Position(-1, 0),
                    new West()
                )
            ],
        ];
    }
}
