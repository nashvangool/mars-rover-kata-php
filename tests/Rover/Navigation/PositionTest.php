<?php
declare(strict_types=1);

namespace Test\Rover\Navigation;

use App\Rover\Navigation\Position;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Position
 */
class PositionTest extends TestCase
{
    public function testInitialisationSetsCorrectCoordinates(): void
    {
        $position = new Position(0, 1);

        $this->assertSame(0, $position->getX());
        $this->assertSame(1, $position->getY());
    }

    public function testStringRepresentation(): void
    {
        $position = new Position(1, 2);

        $this->assertSame('(1,2)', (string) $position);
    }
}
