<?php
declare(strict_types=1);

namespace Test\Rover\Navigation;

use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Position;
use App\Rover\Navigation\Vector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Vector
 */
class VectorTest extends TestCase
{
    public function testPositionAndHeadingArePreservedAfterInitialisation(): void
    {
        $vector = new Vector(
            new Position(1, 3),
            new North()
        );

        $this->assertEquals(
            new Position(1, 3),
            $vector->getPosition()
        );
        $this->assertEquals(
            new North(),
            $vector->getHeading()
        );
    }

    public function testStringRepresentation(): void
    {
        $vector = new Vector(
            new Position(1, 3),
            new North()
        );

        $this->assertSame(
            '(1,3) NORTH',
            (string) $vector
        );
    }
}
