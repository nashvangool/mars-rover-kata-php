<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Utilities;

use App\Rover\Navigation\Heading;
use App\Rover\Navigation\Utilities\HeadingParser;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Utilities\HeadingParser
 */
class HeadingParserTest extends TestCase
{
    public function testEmptyStringIsRejectedWithException(): void
    {
        $parser = new HeadingParser();

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Empty string cannot be converted to a heading');

        $parser->parse('');
    }

    public function testInvalidHeadingStringIsRejectedWithException(): void
    {
        $parser = new HeadingParser();

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid heading string: 'LOLNO'");

        $parser->parse('LOLNO');
    }

    /**
     * @dataProvider provideLongFormHeadings
     */
    public function testValidLongFormHeadingStringsResultInCorrectHeadings(
        string $headingString,
        Heading $expectedHeading
    ): void {
        $parser = new HeadingParser();

        $this->assertEquals(
            $expectedHeading,
            $parser->parse(\strtoupper($headingString)),
            "Fully uppercased heading string '{$headingString}' did not result in the expected heading"
        );
        $this->assertEquals(
            $expectedHeading,
            $parser->parse(\strtolower($headingString)),
            "Fully lowercased heading string '{$headingString}' did not result in the expected heading"
        );
        $this->assertEquals(
            $expectedHeading,
            $parser->parse(\ucfirst($headingString)),
            "Mixed case heading string '{$headingString}' did not result in the expected heading"
        );
    }

    /**
     * @dataProvider provideShortFormHeadings
     */
    public function testValidShortFormHeadingStringsResultInCorrectHeadings(
        string $headingString,
        Heading $expectedHeading
    ): void {
        $parser = new HeadingParser();

        $this->assertEquals(
            $expectedHeading,
            $parser->parse(\strtoupper($headingString)),
            "Uppercase heading string '{$headingString}' did not result in the expected heading"
        );
        $this->assertEquals(
            $expectedHeading,
            $parser->parse(\strtolower($headingString)),
            "Lowercase heading string '{$headingString}' did not result in the expected heading"
        );
    }

    /**
     * @return array<string, array{string, Heading}>
     */
    public function provideLongFormHeadings(): array
    {
        return [
            'North' => [
                'NORTH',
                new Heading\North()
            ],
            'East' => [
                'EAST',
                new Heading\East()
            ],
            'South' => [
                'SOUTH',
                new Heading\South()
            ],
            'West' => [
                'WEST',
                new Heading\West()
            ]
        ];
    }

    /**
     * @return array<string, array{string, Heading}>
     */
    public function provideShortFormHeadings(): array
    {
        return [
            'North' => [
                'N',
                new Heading\North()
            ],
            'East' => [
                'E',
                new Heading\East()
            ],
            'South' => [
                'S',
                new Heading\South()
            ],
            'West' => [
                'W',
                new Heading\West()
            ]
        ];
    }
}
