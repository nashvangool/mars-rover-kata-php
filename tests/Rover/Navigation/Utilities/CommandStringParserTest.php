<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Utilities;

use App\Rover\Navigation\Instruction;
use App\Rover\Navigation\Instruction\Back;
use App\Rover\Navigation\Instruction\Forward;
use App\Rover\Navigation\Instruction\Left;
use App\Rover\Navigation\Instruction\Right;
use App\Rover\Navigation\Utilities\CommandStringParser;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Utilities\CommandStringParser
 */
class CommandStringParserTest extends TestCase
{
    /**
     * @dataProvider provideCommandStringsToParse
     * @param Instruction[] $expectedResult
     */
    public function testTransformationOfStringToInstructionsProducesTheCorrectInstructions(
        string $commandString,
        array $expectedResult
    ): void {
        $parser = new CommandStringParser();

        $instructions = $parser->parse($commandString);

        $this->assertCount(
            \count($expectedResult),
            $instructions,
            'Incorrect amount of instructions from command string parsing!'
        );


        $index = 0;
        foreach ($instructions as $instruction) {
            $this->assertEquals(
                $expectedResult[$index],
                $instruction,
                "Parsing string '{$commandString}' did not produce the expected value at index {$index}"
            );

            $index++;
        }
    }

    /**
     * @return array<string, array{string, Instruction[]}>
     */
    public function provideCommandStringsToParse(): array
    {
        return [
            'Empty command string' => [
                '',
                []
            ],
            'Single instruction' => [
                'F',
                [
                    new Forward()
                ]
            ],
            'Multiple valid instructions' => [
                'FBLR',
                [
                    new Forward(),
                    new Back(),
                    new Left(),
                    new Right()
                ]
            ],
            'Invalid instructions mixed in' => [
                'FOOBAR LOL',
                [
                    new Forward(),
                    new Back(),
                    new Right(),
                    new Left(),
                    new Left()
                ]
            ],
            'Only invalid instructions' => [
                'AAAAAAAAAAAAAAAAA',
                []
            ]
        ];
    }
}
