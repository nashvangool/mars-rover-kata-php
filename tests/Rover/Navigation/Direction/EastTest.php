<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Direction;

use App\Rover\Navigation\Heading\East;
use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Model\CoordinateTransformationVector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Heading\East
 */
class EastTest extends TestCase
{
    public function testTurningLeft(): void
    {
        $east = new East();

        $this->assertEquals(
            new North(),
            $east->turnLeft()
        );
    }

    public function testTurningRight(): void
    {
        $east = new East();

        $this->assertEquals(
            new South(),
            $east->turnRight()
        );
    }

    public function testStringRepresentation(): void
    {
        $this->assertSame(
            'EAST',
            (string) (new East())
        );
    }

    public function testCoordinateDisplacementFactor(): void
    {
        $this->assertEquals(
            new CoordinateTransformationVector(1, 0),
            (new East())->getCoordinateTransformationFactors()
        );
    }
}
