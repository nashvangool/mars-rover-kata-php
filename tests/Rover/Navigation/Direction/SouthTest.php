<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Direction;

use App\Rover\Navigation\Heading\East;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Heading\West;
use App\Rover\Navigation\Model\CoordinateTransformationVector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Heading\South
 */
class SouthTest extends TestCase
{
    public function testTurningLeft(): void
    {
        $south = new South();

        $this->assertEquals(
            new East(),
            $south->turnLeft()
        );
    }

    public function testTurningRight(): void
    {
        $south = new South();

        $this->assertEquals(
            new West(),
            $south->turnRight()
        );
    }

    public function testStringRepresentation(): void
    {
        $this->assertSame(
            'SOUTH',
            (string) (new South())
        );
    }

    public function testCoordinateDisplacementFactor(): void
    {
        $this->assertEquals(
            new CoordinateTransformationVector(0, -1),
            (new South())->getCoordinateTransformationFactors()
        );
    }
}
