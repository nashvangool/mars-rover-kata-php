<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Direction;

use App\Rover\Navigation\Heading\East;
use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Heading\West;
use App\Rover\Navigation\Model\CoordinateTransformationVector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Heading\North
 */
class NorthTest extends TestCase
{
    public function testTurningLeft(): void
    {
        $north = new North();

        $this->assertEquals(
            new West(),
            $north->turnLeft()
        );
    }

    public function testTurningRight(): void
    {
        $north = new North();

        $this->assertEquals(
            new East(),
            $north->turnRight()
        );
    }

    public function testStringRepresentation(): void
    {
        $this->assertSame(
            'NORTH',
            (string) (new North())
        );
    }

    public function testCoordinateDisplacementFactor(): void
    {
        $this->assertEquals(
            new CoordinateTransformationVector(0, 1),
            (new North())->getCoordinateTransformationFactors()
        );
    }
}
