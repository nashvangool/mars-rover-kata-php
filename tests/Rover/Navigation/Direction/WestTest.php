<?php
declare(strict_types=1);

namespace Test\Rover\Navigation\Direction;

use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Heading\West;
use App\Rover\Navigation\Model\CoordinateTransformationVector;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Navigation\Heading\West
 */
class WestTest extends TestCase
{
    public function testTurningLeft(): void
    {
        $west = new West();

        $this->assertEquals(
            new South(),
            $west->turnLeft()
        );
    }

    public function testTurningRight(): void
    {
        $west = new West();

        $this->assertEquals(
            new North(),
            $west->turnRight()
        );
    }

    public function testStringRepresentation(): void
    {
        $this->assertSame(
            'WEST',
            (string) (new West())
        );
    }
    public function testCoordinateDisplacementFactor(): void
    {
        $this->assertEquals(
            new CoordinateTransformationVector(-1, 0),
            (new West())->getCoordinateTransformationFactors()
        );
    }

}
