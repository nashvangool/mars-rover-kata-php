<?php
declare(strict_types=1);

namespace Test\Rover;

use App\Rover\Navigation\CommandString;
use App\Rover\Navigation\Heading\East;
use App\Rover\Navigation\Heading\North;
use App\Rover\Navigation\Heading\South;
use App\Rover\Navigation\Heading\West;
use App\Rover\Navigation\Instruction\Back;
use App\Rover\Navigation\Instruction\Forward;
use App\Rover\Navigation\Instruction\Left;
use App\Rover\Navigation\Instruction\Right;
use App\Rover\Navigation\Position;
use App\Rover\Navigation\Vector;
use App\Rover\Rover;
use mysql_xdevapi\Table;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Rover\Rover
 */
class RoverTest extends TestCase
{
    public function testItPreservesVectorOnInitialisation(): void
    {
        $rover = new Rover(
            new Vector(
                new Position(0, 1),
                new North()
            )
        );

        $this->assertEquals(
            new Vector(
                new Position(0, 1),
                new North()
            ),
            $rover->getVector()
        );
    }

    /**
     * @dataProvider provideRoverParameters
     */
    public function testCommandStringResultsInCorrectVector(
        CommandString $commandString,
        Vector $expectedResultVector
    ): void
    {
        $rover = new Rover(new Vector(
            new Position(0,0),
            new North()
        ));

        $rover->runCommandString($commandString);

        $this->assertEquals($expectedResultVector, $rover->getVector());
    }

    /**
     * @return array<string, array{CommandString, Vector}>
     */
    public function provideRoverParameters(): array
    {
        return [
            'empty command string' => [
                new CommandString(),
                new Vector(
                    new Position(0,0),
                    new North()
                )
            ],
            '180 degree spin right' => [
                new CommandString(
                    new Right(),
                    new Right()
                ),
                new Vector(
                    new Position(0,0),
                    new South()
                )
            ],
            '180 degree spin left' => [
                new CommandString(
                    new Left(),
                    new Left()
                ),
                new Vector(
                    new Position(0,0),
                    new South()
                )
            ],
            'Drive in a circle' => [
                new CommandString(
                    new Forward(),
                    new Left(),
                    new Forward(),
                    new Left(),
                    new Forward(),
                    new Left(),
                    new Forward()
                ),
                new Vector(
                    new Position(0,0),
                    new East()
                )
            ],
            'Complex command string' => [
                new CommandString(
                    new Forward(),
                    new Forward(),
                    new Left(),
                    new Back(),
                    new Forward(),
                    new Forward(),
                    new Right(),
                    new Back(),
                    new Right(),
                    new Back(),
                    new Left(),
                    new Left(),
                    new Forward()
                ),
                new Vector(
                    new Position(-3,1),
                    new West()
                )
            ]
        ];
    }
}
