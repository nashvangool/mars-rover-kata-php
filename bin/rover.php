<?php

use App\Rover\Navigation\Position;
use App\Rover\Navigation\Utilities\CommandStringParser;
use App\Rover\Navigation\Utilities\HeadingParser;
use App\Rover\Navigation\Vector;
use App\Rover\Rover;

require_once __DIR__ . '/../vendor/autoload.php';

function print_help() {
    echo  <<<USAGE
        Usage: rover.php <X coordinate> <Y coordinate> <initial heading> [command string]

        The X and Y coordinates should be integer numbers.
        The initial should be one of `NORTH`, `EAST`, `SOUTH` or `WEST`,
        with the optional shorthands `N`, `E`, `S`, and `W`.

        The result of the application will be the rover's new coordinates and heading.
        USAGE;
    echo PHP_EOL;
}

if ($argc < 4 || $argc > 5) {
    echo 'Incorrect number of arguments supplied' . PHP_EOL . PHP_EOL;
    print_help();
    exit(2);
}

$xCoordinate = (int) $argv[1];
$yCoordinate = (int) $argv[2];
$initialHeading = $argv[3];
$commandString = $argc === 5 ? $argv[4] : '';

try {
    $initialPosition = new Position($xCoordinate, $yCoordinate);
    $initialHeading = (new HeadingParser())->parse($initialHeading);
    $commandString = (new CommandStringParser())->parse($commandString);

    echo 'Touching down rover at ' . $initialPosition . ' with heading ' . $initialHeading . PHP_EOL;

    $rover = new Rover(
        new Vector(
            $initialPosition,
            $initialHeading
        )
    );

    $rover->runCommandString($commandString);

    echo 'New position and heading: ' . $rover->getVector() . PHP_EOL;
} catch (\Exception $exception) {
    echo 'An error occurred: ' . $exception->getMessage() . PHP_EOL;
}

