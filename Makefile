.DEFAULT_GOAL := help

PROJECT_PATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

USER := "$(shell id -u):$(shell id -g)"

DOCKER_RUN = docker run \
	--user $(USER) \
	--rm \
	--volume "$(PROJECT_PATH):/app" \
	--workdir /app \
	--tty \
	--interactive \
	--

PHP := $(DOCKER_RUN) mars-rover-devel:latest php
COMPOSER := $(DOCKER_RUN) composer:2
PHPUNIT := $(PHP) vendor/bin/phpunit
PHPSTAN := $(PHP) vendor/bin/phpstan
INFECTION := $(PHP) vendor/bin/infection

TEST_FILES := $(shell find tests/ -type f -name '*.php')
SOURCE_FILES := $(shell find src/ -type f -name '*.php')

vendor/composer/installed.json: composer.json composer.lock
	@$(COMPOSER) install

.PHONY: composer-exec
composer-exec:
	@$(COMPOSER) $(ARGS)

.PHONY: php-exec
php-exec:
	@$(PHP) $(ARGS)

docker/.devel-built: docker/Dockerfile
	docker build --target devel --tag mars-rover-devel:latest -- docker/
	@touch $@

# Convenience targets

.PHONY: help
# From https://gist.github.com/klmr/575726c7e05d8780505a#gistcomment-2858004
## Show this help
help:
	@echo "$$(tput bold)Available convenience targets:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=29 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'

.PHONY: install
## Install dependencies from composer.lock
install: vendor/composer/installed.json

.PHONY: update
## Update composer.lock and install new package versions
update:
	@$(COMPOSER) update

coverage.txt: $(TEST_FILES) $(SOURCE_FILES) phpunit.xml docker/.devel-built
	@$(PHPUNIT) tests/

.PHONY: test
## Run the test suite and generate coverage report
test: coverage.txt

.phpstan-run: $(SOURCE_FILES) $(TEST_FILES) phpstan.neon docker/.devel-built
	@$(PHPSTAN) analyse -c phpstan.neon
	touch $@

.PHONY: phpstan
## Run PHPStan
phpstan: .phpstan-run

infection.log: coverage.txt infection.json5
	@$(INFECTION) --show-mutations --skip-initial-tests --coverage=coverage-xml/

.PHONY: infection
## Run mutation tests
infection: infection.log

qa: phpstan test infection

.PHONY: clean
## Revert the project to a clean state
clean:
	rm -rf .tested docker/.devel-built vendor .phpunit.cache coverage coverage.txt
